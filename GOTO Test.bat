@ECHO off

cls

:start
echo .
echo .
ECHO [1] FIRST OPTION
ECHO [2] SECOND OPTION
ECHO [3] THIRD OPTION
ECHO [4] RESTART BAT
ECHO [5] QUIT
set choice=
set /p choice=Type the number to print text.
if not '%choice%'=='' set choice=%choice:~0,1%
if '%choice%'=='1' goto hello
if '%choice%'=='2' goto bye
if '%choice%'=='3' goto test
if '%choice%'=='4' goto restart
if '%choice%'=='5' goto quit
ECHO "%choice%" is not valid, try again
ECHO.
goto start

:hello
cls
ECHO ESCOLHEU A PRIMEIRA OP��O
goto end

:bye
cls
ECHO ESCOLHEU A SEGUNDA OP��O
goto end

:test
cls
ECHO ESCOLHEU A TERCEIRA OP��O
goto end

:restart
goto start

:end
goto start

:quit
exit