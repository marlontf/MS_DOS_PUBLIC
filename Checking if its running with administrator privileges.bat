@echo off
goto check_Permissions

:check_Permissions
    echo Permiss�o administrativa requerida. Detectando permiss�o...

    net session >nul 2>&1
    if %errorLevel% == 0 (
        echo Sucesso: Permiss�o administrativa confimada
    ) else (
        echo Falha: Permiss�o administrativa n�o encontrada.
		echo Favor clicar com o bot�o direito sobre o arquivo e escolher a op��o "Executar como Administrador"
    )

    pause >nul